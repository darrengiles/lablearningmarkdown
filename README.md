# LabLearningMarkdown



# Sade Promise
![Promise Album Cover](https://pixhost.icu/avaxhome/6e/2c/003e2c6e.jpg)

> "A golden thread inside of the web\ 
> That I got caught in\ 
> Oh, it's a lover's revenge\ 
> But out of the pain come the best things"

---

An incredible album start to finish. Sade sings beautifully about love, loss, and the fear that often comes associated with them. Her sophomore album showcases a woman still standing strong amidst all that life throws at her. All songs are classics. Best album of all time.

## Track List
1. Is it a Crime?
2. The Sweetest Taboo
3. War of the Hearts
4. Jezebel
5. Mr. Wrong
6. Never as Good as the First Time
7. Fear
8. Tar Baby
9. Maureen

## Genres
- Smooth Jazz (Quiet Storm)
- Soul


##  Sade Disography

| Album | Year |
| --- | ------ |
| Diamond Life | 1984 |
| Promise | 1985 |
| Stronger Than Pride | 1988 |
| Love Deluxe | 1992 |
| Lover's Rock | 2000 |
| Soldier of Love | 2010 |
